﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;

public class StandaloneSpeedSetupController : MonoBehaviour
{
#pragma warning disable CS0414, CS0649 // Never used & never assigned warning (unity)
    public static event Action<float> ReactionSpeedWasUpdated;
    public static string lastActiveSceneName;

    [Serializable]
    protected class SetupStep
    {
        [Header("Language File Key/Index")]
        public string textTitleKey;
        public string textKey;
    }

    [Header("Settings", order = 1)]
    [SerializeField] protected int _setupVersion = 0;
    [SerializeField] protected RectTransform _canvas;
    [SerializeField] protected TextMeshProUGUI _tMPTitle;
    [SerializeField] protected TextMeshProUGUI _tMPText;
    [SerializeField] protected float _defaultTextDuration = 3f;
    [SerializeField] protected Color _defaultTextColor;

    [Header("Setup Steps", order = 2)]
    [SerializeField] protected List<SetupStep> _setupSteps;

    [Header("User Feedback", order = 3)]
    [SerializeField] protected float _warningDuration = 3f;
    [SerializeField] protected Color _warningTextColor;

    [Header("Speed Steps", order = 4)]
    [SerializeField] protected int _speedMenuStepIndex = 1;

    [Header("Reaction Speed Menu", order = 5)]
    [SerializeField, ReadOnly]
    [Tooltip("Not yet implemented")] protected bool _enableSpeedTest = false;
    [Space]
    [SerializeField] protected TextMeshProUGUI _primaryText;
    [SerializeField] protected TextMeshProUGUI _secondaryText;
    [SerializeField] protected RectTransform _primaryImageRect;
    [SerializeField] protected RectTransform _secondaryImageRect;
    [Space]
    [SerializeField] protected GameObject _menuItemPrefab;
    [SerializeField] protected Transform _menuParentReference;
    [SerializeField] protected Color _activeRingColor;
    [SerializeField] protected Color _activeTextColor;
    [SerializeField] protected Color _inactiveColor;
    [SerializeField] protected Color _timerBarFinalColor = new Color(0, 0, 0, 1);
    [SerializeField] protected char _speedUnit = 's';
    [SerializeField] protected List<float> _reactionSpeeds = new List<float>()
    {
        2, 4, 6, 8, 10, 12
    };

    [Header("Tween References", order = 6)]
    [SerializeField] protected RectTransform _screenContainer;
    [SerializeField] protected RectTransform _textContainer;
    [SerializeField] protected RectTransform _menuContainer;
    [SerializeField] protected RectTransform _selectionContainer;
    [SerializeField] protected RectTransform _selectionMoveTarget;
    [SerializeField] protected RectTransform _inputContainer;
    [SerializeField] protected RectTransform _timerBar;

    [Header("Debug", order = 7)]
    [SerializeField, ReadOnly] protected float _setupStartTime;
    [SerializeField, ReadOnly] protected List<float> _manualSpeeds;
    [SerializeField, ReadOnly] protected List<GameObject> _menuItems;
    [SerializeField, ReadOnly] protected int _cursorIndex = -1;
    [SerializeField, ReadOnly] protected float _selectedSpeed = -1;
    [SerializeField, ReadOnly] protected bool _speedClicked = false;
    [SerializeField, ReadOnly] protected bool _primaryClicked = false;
    [SerializeField, ReadOnly] protected bool _secondaryClicked = false;

    protected Coroutine activeMenuCoroutine, activeConfirmTimerCoroutine, _currentSetupRoutine, _currentUpdateTextRoutine, _currentWarningRoutine;
    protected Sequence _activeSequence, _buttonTextChangeSequence, _primaryPunch, _secondaryPunch;
    protected Tween _warningPunchTween;

    protected int _currentStepIndex = -1;
#pragma warning restore CS0414, CS0649 // Never used & never assigned warning (unity)

    public static void LoadSpeedSetup()
    {
        lastActiveSceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene("StandaloneSetupSpeedScene");
    }

    protected void Awake()
    {
        // Prepare the scene
        _tMPText.text = "";
        _tMPTitle.text = "";

        _cursorIndex = -1;
        _selectedSpeed = -1;

        _menuContainer.gameObject.SetActive(false);
        _selectionContainer.gameObject.SetActive(false);
        _menuContainer.gameObject.SetActive(false);
        _timerBar.gameObject.SetActive(false);
        _inputContainer.gameObject.SetActive(false);

        CreateFadeAllButtonTextSequence(0, 0).Complete();
    }

    protected void Start()
    {
        _setupStartTime = Time.realtimeSinceStartup;
        StartCoroutine(StartController());
    }

    protected void Update()
    {
        if (WebGLPreferences.Current.GetPrimaryKeyDown)
        {
            AnimatePrimaryPunch();
        }
        if (WebGLPreferences.Current.GetSecondaryKeyDown)
        {
            AnimateSecondaryPunch();
        }
    }

    protected void AnimatePrimaryPunch()
    {
        if (_primaryPunch != null && _primaryPunch.IsActive()) _primaryPunch.Kill();
        (_primaryPunch = InputManager.CreateInputPunchSequence(_primaryImageRect, _primaryText.rectTransform)).Play();
    }

    protected void AnimateSecondaryPunch()
    {
        if (_secondaryPunch != null && _secondaryPunch.IsActive()) _secondaryPunch.Kill();
        (_secondaryPunch = InputManager.CreateInputPunchSequence(_secondaryImageRect, _secondaryText.rectTransform)).Play();
    }

    protected IEnumerator StartController()
    {
        yield return SpeedSetupCoroutine();
    }

    protected IEnumerator StopController()
    {
        yield return (CreateCanvasFaderSequence(_screenContainer)
            .Join(CreateFadeAllButtonTextSequence(0, OptionsManager.TransitionDuration))
            ).Play().WaitForCompletion();
    }

    protected IEnumerator SpeedSetupCoroutine()
    {
        _inputContainer.gameObject.SetActive(true);
        yield return FadeOutAllText(0);

        // Start iterating over the setup steps
        for (_currentStepIndex = 0; _currentStepIndex < _setupSteps.Count; _currentStepIndex++)
        {
            yield return StartNewSetupRoutine(StepCoroutine(_currentStepIndex));
        }
    }

    protected IEnumerator StepCoroutine(int index)
    {
        SetupStep s = _setupSteps[index];

        // Update Text display
        StartNewTextUpdateRoutine(s);
        StartCoroutine(FadeInAllText());

        if (index == _speedMenuStepIndex) // wait on menu selection
        {
            InstantiateMenuItems();
            yield return AnimateMenuAppear();
            activeMenuCoroutine = StartCoroutine(IterateOverMenuOptions());
            _speedClicked = false;
            while (!WebGLPreferences.Current.GetPrimaryKeyDown && !_primaryClicked && !_speedClicked)
            {
                if (WebGLPreferences.Current.GetSecondaryKeyDown || _secondaryClicked)
                {
                    yield return RedoCurrentSetup();
                }
                yield return null;
            }
            DOTween.KillAll();
            StopCoroutine(activeMenuCoroutine);

            // Select highlighted speed
            if (_enableSpeedTest && _cursorIndex == _menuItems.Count - 1)
            {
                // Start manual speed test
            }
            else
            {
                _selectedSpeed = _reactionSpeeds[_cursorIndex];
            }
            yield return AnimateItemSelect();
        }
        else if (index == _speedMenuStepIndex + 1) // give user choice to accept or change speed setup
        {
            yield return AnimateConfirmAppear();
            activeConfirmTimerCoroutine = StartCoroutine(SetupConfirmTimerCoroutine());

            while (_selectedSpeed > 0)
            {
                if (WebGLPreferences.Current.GetPrimaryKeyDown || _primaryClicked) // success, go to next setup
                {
                    StopCoroutine(activeConfirmTimerCoroutine);
                    DOTween.KillAll();
                    yield return AnimateSetupFinish();
                    yield return FinishSpeedSetup(_selectedSpeed);
                }
                else if (WebGLPreferences.Current.GetSecondaryKeyDown || _secondaryClicked) // select different speed
                {
                    _selectedSpeed = -1;
                }
                yield return null;
            }

            if (_selectedSpeed <= 0)
            {
                // Go back to speed menu selection if user doesn't confirm their time
                _currentStepIndex = _speedMenuStepIndex - 1;

                StopCoroutine(activeConfirmTimerCoroutine);
                DOTween.KillAll();

                yield return AnimateReturnToSpeedMenu();
            }
        }
        else
        {
            StartCoroutine(ChangeButtonTextCoroutine("continue", "back", OptionsManager.TransitionDuration));

            while (!WebGLPreferences.Current.GetPrimaryKeyDown && !_primaryClicked)
            {
                if (WebGLPreferences.Current.GetSecondaryKeyDown || _secondaryClicked)
                {
                    CreateCanvasFaderSequence(_primaryImageRect)
                        .Join(CreateCanvasFaderSequence(_secondaryImageRect))
                        .Play();
                    yield return ReturnToLastScene();
                        
                }
                yield return null;
            }
            yield return _currentUpdateTextRoutine;
        }

        yield return FadeOutAllText();
    }

    protected IEnumerator ReturnToLastScene()
    {
        yield return SceneManager.LoadSceneAsync(lastActiveSceneName);
    }

    protected virtual IEnumerator FinishSpeedSetup(float selectedSpeed)
    {
        print("Finishing speed setup");

        ReactionSpeedWasUpdated?.Invoke(selectedSpeed);

        yield return ReturnToLastScene();
    }

    protected IEnumerator RedoCurrentSetup()
    {
        yield return SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    public void PrimaryClick()
    {
        AnimatePrimaryPunch();

        if (_primaryClicked || _secondaryClicked) return;
        _primaryClicked = true;
    }

    public void SecondaryClick()
    {
        AnimateSecondaryPunch();

        if (_primaryClicked || _secondaryClicked) return;
        _secondaryClicked = true;
    }

    protected void InstantiateMenuItems()
    {
        // Generate menu items
        _menuItems = new List<GameObject>();
        foreach (Transform child in _menuParentReference)
        {
            Destroy(child.gameObject);
        }
        for (int i = 0; i < _reactionSpeeds.Count; i++)
        {
            var item = Instantiate(_menuItemPrefab, _menuParentReference);
            item.GetComponentInChildren<TextMeshProUGUI>().text = _reactionSpeeds[i].ToString() + _speedUnit;

            var index = i;
            item.GetComponent<Button>().onClick.AddListener(() => ClickSpeedItem(index));

            _menuItems.Add(item);
        }
        if (_enableSpeedTest)
        {
            var item = Instantiate(_menuItemPrefab, _menuParentReference);
            item.GetComponentInChildren<TextMeshProUGUI>().text = "...";
            _menuItems.Add(item);
        }
        UpdateMenuDisplay(-1, true);
    }

    protected void ClickSpeedItem(int index)
    {
        if (_speedClicked) return;
        _speedClicked = true;
        _cursorIndex = index;
    }

    protected IEnumerator AnimateReturnToSpeedMenu()
    {
        var selectedItemText = _selectionContainer.GetComponentInChildren<TextMeshProUGUI>(true);
        var selectedItemBackground = _selectionContainer.GetComponentInChildren<Image>(true);
        var selectedItemIndicator = selectedItemBackground.GetComponentsInChildren<Image>(true)
            .FirstOrDefault(x => x != selectedItemBackground);

        selectedItemText.material.DOFade(0, OptionsManager.TransitionDuration);
        selectedItemBackground.material.DOFade(0, OptionsManager.TransitionDuration);
        yield return selectedItemIndicator.material.DOFade(0, OptionsManager.TransitionDuration);
    }

    protected IEnumerator SetupConfirmTimerCoroutine()
    {
        var selectedItemText = _selectionContainer.GetComponentInChildren<TextMeshProUGUI>(true);
        var selectedItemBackground = _selectionContainer.GetComponentInChildren<Image>(true);
        var selectedItemIndicator = selectedItemBackground.GetComponentsInChildren<Image>(true)
            .FirstOrDefault(x => x != selectedItemBackground);

        yield return new WaitForSeconds(2f);

        selectedItemText.material.DOColor(_activeTextColor, OptionsManager.TransitionDuration / 4f);
        selectedItemBackground.material.DOFade(0, OptionsManager.TransitionDuration / 4f);
        selectedItemIndicator.fillAmount = 0;
        selectedItemIndicator.material.DOFade(1, 0f).Complete();

        yield return DOTween.To(() => selectedItemIndicator.fillAmount, x => selectedItemIndicator.fillAmount = x, 1f, _selectedSpeed).WaitForCompletion();

        _selectedSpeed = -1;
    }

    protected IEnumerator AnimateSetupFinish()
    {
        var selectedItemText = _selectionContainer.GetComponentInChildren<TextMeshProUGUI>(true);
        var selectedItemBackground = _selectionContainer.GetComponentInChildren<Image>(true);
        var selectedItemIndicator = selectedItemBackground.GetComponentsInChildren<Image>(true)
            .FirstOrDefault(x => x != selectedItemBackground);

        _timerBar.DOScaleX(0, 0).Complete();
        _timerBar.gameObject.SetActive(true);

        selectedItemText.material.DOFade(0, OptionsManager.TransitionDuration);
        selectedItemBackground.material.DOFade(0, OptionsManager.TransitionDuration / 2f);
        selectedItemIndicator.material.DOFade(1, OptionsManager.TransitionDuration / 2f);
        yield return DOTween.To(() => selectedItemIndicator.fillAmount, x => selectedItemIndicator.fillAmount = x,
            1f, OptionsManager.TransitionDuration / 2f).SetEase(Ease.OutCubic).WaitForCompletion();

        Sequence timerBarFillSequence = DOTween.Sequence();
        timerBarFillSequence.Append(_selectionContainer.DOLocalMove(_selectionMoveTarget.localPosition, OptionsManager.TransitionDuration / 2f).SetRelative().SetEase(Ease.InExpo))
            .Join(_timerBar.DOScaleX(.11f, OptionsManager.TransitionDuration / 2f).SetEase(Ease.InExpo))
            .Append(_timerBar.DOScaleX(1f, OptionsManager.TransitionDuration).SetEase(Ease.OutCirc))
            .Append(_timerBar.GetComponent<Image>().material.DOColor(_timerBarFinalColor, OptionsManager.TransitionDuration / 5f));

        yield return timerBarFillSequence.Play().WaitForCompletion();
    }

    protected IEnumerator AnimateConfirmAppear()
    {
        StartCoroutine(ChangeButtonTextCoroutine("confirm", "back", OptionsManager.TransitionDuration));
        yield return _textContainer.DOLocalJump(new Vector3(0, -190f, 0), 10f, 1, OptionsManager.TransitionDuration / 2f).SetRelative().WaitForCompletion();
        _menuContainer.gameObject.SetActive(false);
    }

    protected IEnumerator AnimateMenuAppear()
    {
        var buttonList = new List<Button>();

        StartCoroutine(ChangeButtonTextCoroutine("select", "back", OptionsManager.TransitionDuration));
        // Slide text up
        yield return _textContainer.DOLocalJump(new Vector3(0, 190f, 0), 10f, 1, OptionsManager.TransitionDuration / 2f).SetRelative().WaitForCompletion();
        _menuContainer.gameObject.SetActive(true);
        _selectionContainer.gameObject.SetActive(false);

        // Show items
        Sequence menuAppearSequence = null;
        for (int i = 0; i < _menuItems.Count; i++)
        {
            Sequence itemAppearSequence = DOTween.Sequence();

            var menuItem = _menuItems[i];
            var menuItemText = menuItem.GetComponentInChildren<TextMeshProUGUI>();
            var menuItemIndicatorBackground = menuItem.transform.GetComponentsInChildren<Image>()[0];
            var menuItemIndicator = menuItemIndicatorBackground.GetComponentsInChildren<Image>()
                .FirstOrDefault(x => x != menuItemIndicatorBackground);

            var button = menuItem.GetComponent<Button>();
            button.interactable = false;
            buttonList.Add(button);

            menuItemIndicator.fillAmount = 0;

            itemAppearSequence
                .Append(menuItemText.material.DOFade(0, OptionsManager.TransitionDuration).From())
                .Join(menuItemIndicatorBackground.material.DOFade(0, OptionsManager.TransitionDuration).From());

            if (menuAppearSequence == null)
            {
                menuAppearSequence = DOTween.Sequence();
                menuAppearSequence.Append(itemAppearSequence);
            }
            else menuAppearSequence.Insert(i * OptionsManager.TransitionDuration / 2f, itemAppearSequence);
        }
        yield return menuAppearSequence.Play().WaitForCompletion();

        foreach (var button in buttonList)
        {
            button.interactable = true;
        }
    }

    protected IEnumerator AnimateItemSelect()
    {
        var selectedItemText = _selectionContainer.GetComponentInChildren<TextMeshProUGUI>(true);
        var selectedItemBackground = _selectionContainer.GetComponentInChildren<Image>(true);
        var selectedItemIndicator = selectedItemBackground.GetComponentsInChildren<Image>(true)
            .FirstOrDefault(x => x != selectedItemBackground);

        Sequence menuDiappearSequence = null;
        for (int i = 0; i < _menuItems.Count; i++)
        {
            Sequence itemDisappearSequence = DOTween.Sequence();

            var menuItem = _menuItems[i];
            var menuItemText = menuItem.GetComponentInChildren<TextMeshProUGUI>();
            var menuItemIndicatorBackground = menuItem.transform.GetComponentsInChildren<Image>()[0];
            var menuItemIndicator = menuItemIndicatorBackground.GetComponentsInChildren<Image>()
                .FirstOrDefault(x => x != menuItemIndicatorBackground);

            var button = menuItem.GetComponent<Button>();
            button.interactable = false;

            if (i == _cursorIndex)
            {
                // Hide item from menu container
                menuItemText.material.DOFade(0, 0).Complete();
                menuItemIndicator.material.DOFade(0, 0).Complete();
                menuItemIndicatorBackground.material.DOFade(0, 0).Complete();

                // Move selection duble to position and mimic its relevant properties
                selectedItemText.material.DOFade(1, 0).Complete();
                selectedItemText.text = menuItemText.text;
                _selectionContainer.transform.position = _menuItems[_cursorIndex].transform.position;
                selectedItemIndicator.material.DOFade(1, 0).Complete();

                _selectionContainer.gameObject.SetActive(true);

                // Fill the rest of the timer quickly
                selectedItemIndicator.fillAmount = menuItemIndicator.fillAmount;
                selectedItemBackground.material.DOFade(0, 0).Complete();
                continue;
            }

            itemDisappearSequence
                .AppendInterval(i * OptionsManager.TransitionDuration / 10f)
                .Append(menuItemText.material.DOFade(0, OptionsManager.TransitionDuration))
                .Join(menuItemIndicator.material.DOFade(0, OptionsManager.TransitionDuration))
                .Join(menuItemIndicatorBackground.material.DOFade(0, OptionsManager.TransitionDuration));

            if (menuDiappearSequence == null)
            {
                menuDiappearSequence = DOTween.Sequence();
                menuDiappearSequence.Append(itemDisappearSequence);
            }
            else
            {
                menuDiappearSequence.Join(itemDisappearSequence);
            }
        }

        yield return DOTween.To(() => selectedItemIndicator.fillAmount, x => selectedItemIndicator.fillAmount = x,
            1f, OptionsManager.TransitionDuration / 2f).SetEase(Ease.OutCubic).WaitForCompletion();
        yield return menuDiappearSequence.Play().WaitForCompletion();

        Sequence selectionSequence = DOTween.Sequence();
        selectionSequence
            .Append(selectedItemIndicator.material.DOFade(0, OptionsManager.TransitionDuration / 2f))
            .Join(selectedItemBackground.material.DOFade(1, OptionsManager.TransitionDuration / 2f))
            .Join(_selectionContainer.DOLocalMove(new Vector3(0, -110f, 0), OptionsManager.TransitionDuration).SetEase(Ease.OutCubic));

        yield return selectionSequence.Play().WaitForCompletion();
    }

    protected IEnumerator IterateOverMenuOptions()
    {
        MoveMenuCursor(_cursorIndex);
        UpdateMenuDisplay();
        while (true)
        {
            yield return AnimateMenuItem();
            MoveMenuCursor();
        }
    }

    protected IEnumerator AnimateMenuItem()
    {
        var time = _reactionSpeeds[Mathf.Clamp(_cursorIndex, 0, _reactionSpeeds.Count - 1)];

        var menuItemIndicatorBackground = _menuItems[_cursorIndex].transform.GetComponentsInChildren<Image>()[0];
        var menuItemIndicator = menuItemIndicatorBackground.transform.GetComponentsInChildren<Image>()
            .FirstOrDefault(x => x != menuItemIndicatorBackground);
        yield return DOTween.To(() => menuItemIndicator.fillAmount, x => menuItemIndicator.fillAmount = x,
                            1f, time).WaitForCompletion();
    }

    protected void MoveMenuCursor(int toPosition = -1)
    {
        var lastPos = _cursorIndex;
        _cursorIndex = (toPosition < 0 || toPosition >= _menuItems.Count) ? (_cursorIndex + 1) % _menuItems.Count : toPosition;
        Mathf.Clamp(_cursorIndex, 0, _menuItems.Count - 1);
        UpdateMenuDisplay(lastPos);
    }

    protected IEnumerator ChangeButtonTextCoroutine(string primaryTextKey, string secondaryTextKey, float transitionTime)
    {
        _primaryClicked = false;
        _secondaryClicked = false;

        yield return FadeAllButtonText(0, transitionTime / 2);
        var primaryText = I2.Loc.LocalizationManager.GetTranslation(primaryTextKey);
        var secondaryText = I2.Loc.LocalizationManager.GetTranslation(secondaryTextKey);
        if (primaryText == "Not found" && secondaryText == "Not found") yield break;

        _primaryText.text = primaryText;
        _secondaryText.text = secondaryText;
        yield return FadeAllButtonText(1, transitionTime / 2);
    }

    protected IEnumerator FadeAllButtonText(float fadeLevel, float duration = -1f)
    {
        duration = duration < 0 ? OptionsManager.TransitionDuration : duration;
        _buttonTextChangeSequence?.Kill();

        _buttonTextChangeSequence = CreateFadeAllButtonTextSequence(fadeLevel, duration);
        yield return _buttonTextChangeSequence.Play().WaitForCompletion();
    }

    protected Sequence CreateFadeAllButtonTextSequence(float fadeLevel, float duration)
    {
        var sequence = DOTween.Sequence();
        if (_primaryText.color.a != fadeLevel)
            sequence.Join(_primaryText.material.DOFade(fadeLevel, duration));

        if (_secondaryText.color.a != fadeLevel)
            sequence.Join(_secondaryText.material.DOFade(fadeLevel, duration));

        return sequence;
    }

    protected void UpdateMenuDisplay(int lastPos = -1, bool ignoreCursorPos = false)
    {
        // Update display and indicator
        for (int i = 0; i < _menuItems.Count; i++)
        {
            var menuItemText = _menuItems[i].GetComponentInChildren<TextMeshProUGUI>();

            var menuItemIndicatorBackground = _menuItems[i].transform.GetComponentsInChildren<Image>()[0];
            var menuItemIndicator = menuItemIndicatorBackground.transform.GetComponentsInChildren<Image>()
                .FirstOrDefault(x => x != menuItemIndicatorBackground);

            if (i == _cursorIndex && !ignoreCursorPos)
            {
                menuItemText.material.DOColor(_activeTextColor, OptionsManager.TransitionDuration / 4f);
                menuItemIndicatorBackground.material.DOFade(0, OptionsManager.TransitionDuration / 4f);
                menuItemIndicator.material.DOFade(1, 0f).Complete();
            }
            else if (i == lastPos)
            {
                menuItemText.material.DOColor(_inactiveColor, OptionsManager.TransitionDuration / 2f);
                menuItemIndicatorBackground.material.DOFade(1, OptionsManager.TransitionDuration / 2f);
                menuItemIndicator.material.DOFade(0, OptionsManager.TransitionDuration / 2f);
            }
            else
            {
                menuItemText.color = _inactiveColor;
                menuItemIndicator.fillAmount = 0;
            }
        }
    }

    protected Coroutine StartNewSetupRoutine(IEnumerator newCoroutine)
    {
        //if (rememberLastRoutine) previousSetupRoutine = currentSetupRoutine;
        if (_currentSetupRoutine != null) StopCoroutine(_currentSetupRoutine);
        if (_currentWarningRoutine != null) StopCoroutine(_currentWarningRoutine);
        return _currentSetupRoutine = StartCoroutine(newCoroutine);
    }

    protected Coroutine StartNewTextUpdateRoutine(SetupStep step)
    {
        if (_currentUpdateTextRoutine != null) StopCoroutine(_currentUpdateTextRoutine);

        return _currentUpdateTextRoutine = StartCoroutine(UpdateTextCoroutine(step));
    }

    protected Coroutine StartNewWarningRoutine(string warningKey, float duration = -1f)
    {
        if (_currentWarningRoutine != null) return _currentWarningRoutine;
        duration = duration < 0 ? OptionsManager.TransitionDuration / 2f : duration;

        return _currentWarningRoutine = StartCoroutine(IssueWarningCoroutine(warningKey, duration));
    }

    protected IEnumerator IssueWarningCoroutine(string warningKey, float transitionDuration)
    {
        yield return FadeOutText(_tMPTitle, transitionDuration);
        yield return UpdateTMPText(_tMPTitle, warningKey, _warningTextColor);
        if (_warningPunchTween != null) _warningPunchTween.Complete();
        _warningPunchTween = _tMPTitle.transform.DOPunchScale(Vector3.one * 0.1f, transitionDuration, 1, 0);
        yield return FadeInText(_tMPTitle, transitionDuration);

        if (_warningDuration <= 0) yield break;
        yield return new WaitForSeconds(_warningDuration);

        yield return FadeOutText(_tMPTitle, transitionDuration);
        yield return UpdateTMPText(_tMPTitle, _setupSteps[_currentStepIndex].textTitleKey);
        yield return FadeInText(_tMPTitle, transitionDuration);

        _currentWarningRoutine = null;
    }

    protected IEnumerator UpdateTextCoroutine(SetupStep step)
    {
        StartCoroutine(UpdateTMPText(_tMPText, step.textKey));
        StartCoroutine(UpdateTMPText(_tMPTitle, step.textTitleKey));
        yield return new WaitForSeconds(_defaultTextDuration);
    }

    protected IEnumerator FadeInAllText(float duration = -1f)
    {
        duration = duration < 0 ? OptionsManager.TransitionDuration : duration;

        StartCoroutine(FadeInText(_tMPTitle, duration));
        yield return StartCoroutine(FadeInText(_tMPText, duration));
    }

    protected IEnumerator FadeOutAllText(float duration = -1f)
    {
        duration = duration < 0 ? OptionsManager.TransitionDuration : duration;

        StartCoroutine(FadeOutText(_tMPTitle, duration));
        yield return StartCoroutine(FadeOutText(_tMPText, duration));
    }

    

    protected IEnumerator UpdateTMPText(TextMeshProUGUI target, string textKey = null, Color color = default)
    {
        var text = I2.Loc.LocalizationManager.GetTranslation(textKey);
        if (color == default) color = _defaultTextColor;
        if (text != null)
        {
            target.text = text;
            color.a = target.color.a;
            target.color = color;
        }
        else
        {
            target.text = "";
        }
        yield break;
    }

    protected IEnumerator FadeOutText(TextMeshProUGUI target, float duration)
    {
        target.material.DOFade(1, 0).Complete();
        yield return target.material.DOFade(0, duration).WaitForCompletion();
    }

    protected IEnumerator FadeInText(TextMeshProUGUI target, float duration)
    {
        target.material.DOFade(0, 0).Complete();
        yield return target.material.DOFade(1, duration).WaitForCompletion();
    }

    protected Sequence CreateCanvasFaderSequence(RectTransform target, float fadeLevel = 0f, float duration = -1f)
    {
        duration = duration < 0 ? OptionsManager.TransitionDuration : duration;

        var sequence = DOTween.Sequence();
        var canvasGroup = target.gameObject.GetComponent<CanvasGroup>();
        if (canvasGroup == null)
        {
            //Debug.Log("Creating canvas group");
            canvasGroup = target.gameObject.AddComponent<CanvasGroup>();
        }

        sequence.Join(DOTween.To(() => canvasGroup.alpha, x => canvasGroup.alpha = x, fadeLevel, duration));
        return sequence;
    }
}
